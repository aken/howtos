Cómo generar un certificado SSL/TLS gratis con certbot 


Certbot es un cliente ACME automático y fácil de utilizar, que obtiene certificados SSL/TLS gratis para tu sitio Web, provistos por Let's Encrypt. Certbot fue desarrollado por la EFF (Electronic Frontier Foundation) como "cliente oficial" de la autoridad certificante de Let's Encrypt, pero también funciona con cualquier CA que soporte el protocolo ACME (Automated Certificate Management Environment).

Certbot (anteriormente el cliente Let's Encrypt letsencrypt-auto) es el cliente recomendado por Let's Encrypt para emitir sus certificados, y opcionalmente auto-configurar HTTPS en tu servidor Web.

Este artículo explica cómo emitir un certificado SSL/TLS gratuito de Let's Encrypt utilizando el cliente certbot utilizando el plugin webroot en Debian 7 (wheezy).



Certbot soporta un número de diferentes plugins que pueden ser utilizados para obtener y/o instalar certificados. Para aquellos usuarios poco experimentados, certbot tiene la capacidad de auto-configurar a la mayoría de los servidores HTTP, a fin de instalar el certificado generado y poner en funcionamiento HTTPS. Sin embargo, si se utiliza una configuración de HTTP/HTTPS personalizada y se cuenta con acceso a la configuración del servidor Web, resultará conveniente realizar la autenticación del dominio (ACME challenge) utilizando el servidor Web actual. Esto tiene la ventaja de no tener que detener el servicio Web durante la generación del certificado.

El modo automático levanta un servidor Web temporal para llevar a cabo la autenticación sin necesidad de intervención del usuario. Pero para ello es necesario detener el servidor corriendo en el puerto 80, lo cual implica un corte en el servicio.

El plugin webroot realiza la autenticación ACME creando un archivo temporal para cada uno de los dominios a certificar (por supuesto, todos estos dominios deben resolver al servidor donde corre certbot). Estos archivos temporales son accedidos inmediatamente por la CA, para verificar la autenticidad y autoridad de los dominios a certificar.

Este es exactamente el mismo proceso que expliqué en el artículo Cómo obtener un certificado SSL gratis de Let's Encrypt.

Antes de comenzar, configurar el servidor Web en cuestión para que el acceso a los archivos en el directorio .well-known/acme-challenge/ sea de tipo MIME "text/plain". Para el caso de servidores Nginx, configurar el directorio, dentro de la configuración del sitio, de la siguiente forma:


    location '/.well-known/acme-challenge' {
        default_type "text/plain";
        root /var/www/linuxito.com;
    }
    

Para el caso de Apache:


    <Directory /var/www/linuxito.com/.well-known/acme-challenge>
        DefaultType text/plain
    </Directory>

Luego recargar la configuración (service nginx reload o service apache2/httpd reload). De esta forma no es necesario detener el servicio Web en ningún momento.
Instalación de certbot

Para los sistemas que no dispongan certbot desde paquete, la instalación es trivial. Simplemente descargar el cliente certbot-auto y otorgarle permisos de ejecución:

root@linuxito:~/scripts# wget https://dl.eff.org/certbot-auto
--2017-03-14 07:11:29--  https://dl.eff.org/certbot-auto
Resolving dl.eff.org (dl.eff.org)... 173.239.79.196
Connecting to dl.eff.org (dl.eff.org)|173.239.79.196|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 46789 (46K) [application/octet-stream]
Saving to: `certbot-auto'

100%[==============================================================================>] 46,789      --.-K/s   in 0.06s   

2017-03-14 07:11:30 (740 KB/s) - `certbot-auto' saved [46789/46789]

root@linuxito:~/scripts# chmod a+x certbot-auto 

Generación de un certificado SSL/TLS utilizando el plugin webroot

La creación del certificado es extremadamente simple, porque el cliente se encarga de crear los archivos con el contenido correcto (antes tenía que realizarlo uno manualmente). Sólo basta con ejecutar:

./certbot-auto certonly

Por ejemplo, generar un certificado TLS para los dominios "linuxito.com" y "www.linuxito.com":

root@linuxito:~/scripts# ./certbot-auto certonly
Saving debug log to /var/log/letsencrypt/letsencrypt.log

En el primer paso, seleccionar "webroot":

How would you like to authenticate with the ACME CA?
-------------------------------------------------------------------------------
1: Place files in webroot directory (webroot)
2: Spin up a temporary webserver (standalone)
-------------------------------------------------------------------------------
Select the appropriate number [1-2] then [enter] (press 'c' to cancel): 1

Luego, especificar una dirección de correo electrónica válida:

Enter email address (used for urgent renewal and security notices) (Enter 'c' to
cancel):***@***.org

Esta dirección será utilizada para notificar al antes de que expire el certificado generado y así poder renovarlo. Recordar que los certificados emitidos por Let's Encrypt tienen una validez de 3 meses.

A continuación, aceptar los términos del servicio:

-------------------------------------------------------------------------------
Please read the Terms of Service at
https://letsencrypt.org/documents/LE-SA-v1.1.1-August-1-2016.pdf. You must agree
in order to register with the ACME server at
https://acme-v01.api.letsencrypt.org/directory
-------------------------------------------------------------------------------
(A)gree/(C)ancel: A

Si se desea, es posible recibir por correo las novedades de la EFF:

-------------------------------------------------------------------------------
Would you be willing to share your email address with the Electronic Frontier
Foundation, a founding partner of the Let's Encrypt project and the non-profit
organization that develops Certbot? We'd like to send you email about EFF and
our work to encrypt the web, protect its users and defend digital rights.
-------------------------------------------------------------------------------
(Y)es/(N)o: N

Luego viene la parte importante, indicar claramente los dominios para los cuales el certificado será válido:

Please enter in your domain name(s) (comma and/or space separated)  (Enter 'c'
to cancel):linuxito.com www.linuxito.com
Obtaining a new certificate
Performing the following challenges:
http-01 challenge for linuxito.com
http-01 challenge for www.linuxito.com

Es posible separar los dominios con coma o espacio en blanco.

Siguiente, especificar la raíz del sitio web ("webroot") relacionado a los dominios a certificar. La primera vez es necesario agregar un webroot:

Select the webroot for linuxito.com:
-------------------------------------------------------------------------------
1: Enter a new webroot
-------------------------------------------------------------------------------
Press 1 [enter] to confirm the selection (press 'c' to cancel): 1
Input the webroot for linuxito.com: (Enter 'c' to cancel):/var/www/linuxito.com

En este caso, la raíz del sitio Web es /var/www/linuxito.com. Una vez agregada, seleccionarla:

Select the webroot for www.linuxito.com:
-------------------------------------------------------------------------------
1: Enter a new webroot
2: /var/www/linuxito.com
-------------------------------------------------------------------------------
Select the appropriate number [1-2] then [enter] (press 'c' to cancel): 2

Finalmente, se genera el nuevo certificado:

Waiting for verification...
Cleaning up challenges
Unable to clean up challenge directory /var/www/linuxito.com/.well-known/acme-challenge
Generating key (2048 bits): /etc/letsencrypt/keys/0000_key-certbot.pem
Creating CSR: /etc/letsencrypt/csr/0000_csr-certbot.pem

IMPORTANT NOTES:
 - Congratulations! Your certificate and chain have been saved at
   /etc/letsencrypt/live/linuxito.com/fullchain.pem. Your cert will
   expire on 2017-06-12. To obtain a new or tweaked version of this
   certificate in the future, simply run certbot-auto again. To
   non-interactively renew *all* of your certificates, run
   "certbot-auto renew"
 - Your account credentials have been saved in your Certbot
   configuration directory at /etc/letsencrypt. You should make a
   secure backup of this folder now. This configuration directory will
   also contain certificates and private keys obtained by Certbot so
   making regular backups of this folder is ideal.
 - If you like Certbot, please consider supporting our work by:

   Donating to ISRG / Let's Encrypt:   https://letsencrypt.org/donate
   Donating to EFF:                    https://eff.org/donate-le

El certificado (cert.pem) y su clave privada (privkey.pem) quedan almacenados en el directorio /etc/letsencrypt/live/linuxito.com/:

root@linuxito:~/scripts# ll /etc/letsencrypt/live/linuxito.com/
total 4
-rw-r--r-- 1 root root 543 Mar 14 07:16 README
lrwxrwxrwx 1 root root  36 Mar 14 07:16 cert.pem -> ../../archive/linuxito.com/cert1.pem
lrwxrwxrwx 1 root root  37 Mar 14 07:16 chain.pem -> ../../archive/linuxito.com/chain1.pem
lrwxrwxrwx 1 root root  41 Mar 14 07:16 fullchain.pem -> ../../archive/linuxito.com/fullchain1.pem
lrwxrwxrwx 1 root root  39 Mar 14 07:16 privkey.pem -> ../../archive/linuxito.com/privkey1.pem

Sólo resta configurar HTTPS en Nginx o Apache, según corresponda. Recuerden que, para asegurarse que el sitio sea confiable en todos los navegadores, es necesario utilizar la cadena completa (fullchain.pem) en lugar de sólo el certificado (cert.pem), junto con la clave privada del certificado (privkey.pem).

Renovar automáticamente todos los cetificados

Certbot tiene la opción de renovar automáticamente (y de forma desatendida) todos los certificados emitidos según la configuración utilizada previamente. Para ello, simplemente se debe ejecutar ./certbot-auto renew.

Es posible hacer una corrida a modo de prueba utilizando la opción --dry-run:

root@linuxito:~/scripts# ./certbot-auto renew --dry-run
Saving debug log to /var/log/letsencrypt/letsencrypt.log

-------------------------------------------------------------------------------
Processing /etc/letsencrypt/renewal/linuxito.com.conf
-------------------------------------------------------------------------------
Cert not due for renewal, but simulating renewal for dry run
Renewing an existing certificate
Performing the following challenges:
http-01 challenge for linuxito.com
http-01 challenge for www.linuxito.com
Waiting for verification...
Cleaning up challenges
Unable to clean up challenge directory /var/www/linuxito.com/.well-known/acme-challenge
Generating key (2048 bits): /etc/letsencrypt/keys/0001_key-certbot.pem
Creating CSR: /etc/letsencrypt/csr/0001_csr-certbot.pem

-------------------------------------------------------------------------------
new certificate deployed without reload, fullchain is
/etc/letsencrypt/live/linuxito.com/fullchain.pem
-------------------------------------------------------------------------------
** DRY RUN: simulating 'certbot renew' close to cert expiry
**          (The test certificates below have not been saved.)

Congratulations, all renewals succeeded. The following certs have been renewed:
  /etc/letsencrypt/live/linuxito.com/fullchain.pem (success)
** DRY RUN: simulating 'certbot renew' close to cert expiry
**          (The test certificates above have not been saved.)

IMPORTANT NOTES:
 - Your account credentials have been saved in your Certbot
   configuration directory at /etc/letsencrypt. You should make a
   secure backup of this folder now. This configuration directory will
   also contain certificates and private keys obtained by Certbot so
   making regular backups of this folder is ideal.

Si funciona correctamente, es posible definir un cronjob que corra de forma periódica y se encargue de las renovaciones:

./path/to/certbot-auto renew --quiet --no-self-upgrade

Esto no requiere reiniciar o recargar la configuración de los servidores Web que utilizan los certificados, pues los archivos dentro de /etc/letsencrypt/live/linuxito.com/ son enlaces simbólicos a los certificados y claves.

Let's Encrypt recomienda correr esta tarea dos veces al día (eligiendo un minuto aleatorio, para aliviar la carga en sus servidores). Tal vez parezca exagerado (siendo que los certificados son válidos por tres meses) pero evitará interrupciones en el servicio en caso de que Let's Encrypt revoque certificados por alguna razón (brecha de seguridad, cambio en algoritmos de hashing, etc.) Tener en cuenta que este trabajo no realiza ninguna tarea si los certificados no están por expirar o revocados.
