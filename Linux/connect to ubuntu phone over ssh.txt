

To activate ssh access entirely over wifi, without developer mode on the phone, without any special tooling on your computer and without using USB:

    If you don't already have an ssh keypair, type ssh-keygen on your computer and follow the instructions to generate one.
    Install the Terminal app on the phone from the App Store.

    Open a Terminal and type (this is easier with the phone in a landscape orientation):

    sudo setprop persist.service.ssh true
    mkdir -pm700 ~/.ssh
    ip addr
    nc -l 1234 > ~/.ssh/authorized_keys

    (the last command will hang; this is expected)

    Look for your phone's IP address in the Terminal as returned by the ip addr command above.

    On your computer, type (replacing 192.0.2.1 with your phone's IP address from above):

    nc 192.0.2.1 1234 < ~/.ssh/id_rsa.pub

    If successful, the last command on your phone's Terminal will now succeed.

    On your computer, type (again replacing 192.0.2.1 with your phone's IP address from above):

    ssh phablet@192.0.2.1

    If your phone's IP address changes, you will need to use ip addr in the Terminal app on your phone again and adjust your ssh command accordingly.



