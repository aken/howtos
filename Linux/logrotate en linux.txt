Un muy breve resumen del funcionamiento de logrotate para que los archivos de LOG de nuestras aplicaciones “roten” cuando consigan un tamaño concreto y así sea más legible para el administrador.

A priori, saber que el logrotate es un demonio que se ejecuta de forma autónoma, pero que si queremos comprobar su funcionamiento, sólo debemos lanzar la siguiente instrucción:

    logrotate -vf /etc/logrotate.conf

Como podemos ver, logrotate se encuentra bajo la carpeta /etc y también debemos saber que es en ese fichero donde se establece la configuración base para cualquier aplicativo que deseemos rotar. Si queremos fijar una configuración concreta para un aplicativo concreto, debemos ir a la carpeta /etc/logrotate.d y crear un archivo nuevo con la configuración específica.

Un ejemplo de configuración específica para un fichero de log que tenemos en un servidor (/etc/logrotate.d/testlink)

    /srv/www/verificacion/testlink/logs/userlog0.log {
    copytruncate
    daily
    rotate 20
    compress
    missingok
    size 10M
    }

Ahí estamos diciendo básicamente que:

    Daily: realice la comprobación cada día
    Compress: Genere un fichero gzip del log rotado
    Size: Que genere el archivo gzip cuando el log userlog0.log ocupe 10 megas
    Rotate: el nº veces que rotará. Cuando complete ese nº de veces, machará el primero y volverá a empezar.
    Missingok: si el fichero de log no existe, no devuelve error.
    Copytruncate: creará una copia del fichero original antes de hacer el rotado, de esa forma evitaremos que el log principal no se quede corrompido y nuestro aplicativo siga funcionando con normalidad. Así siempre generará al menos un log de cero bytes y siempre existirá.

Si queremos comprobar únicamente la configuración establecida para este log, debemos ejecutar:

    logrotate -vf /etc/logrotate.d/testlink
