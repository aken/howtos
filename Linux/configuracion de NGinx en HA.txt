
Configuracion de NGinx en HA

Instalación del sistema 
Partimos de la instalación del sistema propuesta en el siguiente enlace Instalación Nginx/1.6.2 (stable) en Centos7
Para nuestro ejemplo trabajaremos con dos servidores Centos 7 llamados "app01-local" con IP 10.32.20.62, "app02-local" con IP 10.32.20.60, asignaremos una IP virtual para el cluster que será la 10.32.20.254, y al cluster le llamaremos "cluster-web"

Para poder trabajar con el nombre de las maquinas tendremos que añadirlos a "/etc/hosts"
	10.32.20.62 app01-local
	10.32.20.60 app02-local

Instalación de paquetes necesarios en ambos nodos, creación de usuario "hacluster" y arranque del servicio.
	sudo yum install corosync pcs pacemaker
	sudo passwd hacluster (Contraseña del usuario del hacluster)
	sudo systemctl start pcsd (arrancar servicio)

Pasos a realizar solo desde uno de los nodos
	sudo pcs cluster auth app01-local app02-local (autenticamos los nodos para el cluster, cuando pregunte introduciremos el usuario del creado anteriormente para el cluster y su password)
	sudo pcs cluster setup --name cluster_web app01-local app02-local (añadimos los nodos al cluster)
	sudo pcs cluster start --all (arrancamos los servicios de cluster de los nodos)
	sudo pcs status cluster (vemos el estado de los cluster)
	sudo pcs status nodes (vemos el estado de los nodos)
	sudo corosync-cmapctl | grep members
	sudo pcs status corosync
	--Chequeo de errores del cluster
	sudo crm_verify -L -V (veremos un error referente a "stonith" que solucionamos en el siguiente paso)
	sudo pcs property set stonith-enabled=false (soluciona el error que nos ha aparecido)
	sudo pcs property set no-quorum-policy=ignore (Ignoramos el quorum)
	sudo pcs property (comprobamos que todo es correcto)

Configruación de la IP virtual
	sudo pcs resource create virtual_ip ocf:heartbeat:IPaddr2 ip=10.32.20.254 cidr_netmask=32 op monitor interval=30s
	sudo pcs status resources (comprobamos el estado de la ip virtual) 

Detenemos el servicio de Nginx en ambos nodos
	sudo systemctl stop nginx

Continuamos trabajando sobre el primer nodo que habiamos elegido
	sudo pcs resource create webserver ocf:heartbeat:nginx configfile=/etc/nginx/conf.d/default.conf
	statusurl="http://localhost/server-status" op monitor interval=1min --force
	sudo pcs constraint colocation add webserver virtual_ip INFINITY
	sudo pcs constraint order virtual_ip then webserver
	sudo pcs constraint location webserver prefers app01-local=50
	--Reinicio del cluster
	sudo pcs cluster stop --all && sudo pcs cluster start --all

Pasamos todos los servicios necesarios a "enable" en ambos nodos.
	sudo systemctl enable pcsd
	sudo systemctl enable corosync
	sudo systemctl enable pacemaker
	sudo systemctl enable nginx 

Añadimos lo siguiente en "/etc/nginx/nginx.conf" en ambos servidores
	proxy_set_header        X-Real-IP $remote_addr;
			proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;        
		upstream wwwapp01 {
					server app01-local;                
			server app02-local;        
			}
