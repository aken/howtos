Uso del comando SED en Linux y UNIX con ejemplos
Hablamos del popular comando SED, básico en el día a día en sistemas UNIX y GNU/Linux. Su nombre es un acrónimo del inglés de editor de secuencias (“Stream Editor”) y nos permite realizar muchas funciones en archivos como, por ejemplo: buscar, buscar y reemplazar, insertar o eliminar.
Aún así el uso más común de SED en UNIX y GNU/Linux es para sustituir o para buscar y reemplazar. Al utilizar SED, podemos editar archivos, incluso sin abrirlos, de manera individual o masiva. Dicho sea, que esta forma, es mucho más rápida para encontrar y reemplazar algo en un archivo de manera manual.
SED es un potente editor de flujo de texto. Podemos hacer insertar, borrar, buscar y reemplazar.
Dicho comando admite expresiones regulares que le permiten realizar una comparación de patronos complejos.

Uso del comando Sed en Linux
---------------------------------------
Lo cierto es que tiene una curva media de aprendizaje, sobre todo en lo que respecta al uso combinado con las expresiones regulares.
Su sintaxis es la siguiente:
	sed OPCIONES... [SCRIPT] [FICHERO...]
Para ver su funcionamiento, vamos a considerar el siguiente texto:
	“La empresa tiene todos sus sistemas operativos con Microsoft Windows, 
	en sus últimas versiones. Con Microsoft Windows conseguimos alcanzar nuestros objetivos.  
	Microsoft Windows es una empresa que genera confianza.

Reemplazar o sustituir un string
--------------------------------------
El comando Sed se usa principalmente para reemplazar el texto en un archivo. El siguiente comando simple sed reemplaza la palabra “Microsoft Windows” por “GNU Linux” en el archivo.
	$sed 's/Microsoft Windows/GNU Linux/' fichero.txt
De esta manera el resultado es el siguiente:
	“La empresa tiene todos sus sistemas operativos con GNU Linux, 
	en sus últimas versiones. Con Microsoft Windows conseguimos alcanzar nuestros objetivos.  
	Microsoft Windows es una empresa que genera confianza.”
Como se puede observar solo ha hecho el cambio en la primera frase. Antes de continuar, veamos que hemos hecho. Con el parámetro “s” especificamos la operación de sustitución. Los “/” son delimitadores. “Microsoft Windows” es el patrón de búsqueda y el “GNU Linux” es la cadena de reemplazo.
De manera predeterminada, el comando sed reemplaza la primera aparición del patrón en cada línea y no reemplazará la segunda y consecutivas.
NOTA IMPORTANTE: Si queremos que los cambios sean permanentes en el fichero, debemos utilizar el parámetro “-i“, justo después del comando SED.

Reemplazar un número dado de apariciones de un string
-------------------------------------------------------------
Para reemplazar un número dado de apariciones de un string, siempre siguiendo el orden de aparición, debemos usar los indicadores numéricos, para así reemplazar la primera, segunda, etc, apariciones de un patrón en una línea.
Ahora vamos a sustituir la segunda aparición:
	sed 's/Microsoft Windows/GNU Linux/2' fichero.txt
Quedando así:
	"La empresa tiene todos sus sistemas operativos con Microsoft Windows, 
	en sus últimas versiones. Con GNU Linux conseguimos alcanzar nuestros objetivos.  
	Microsoft Windows es una empresa que genera confianza."

Reemplazar todas las apariciones de un string en un texto
-----------------------------------------------------------------
Para reemplazar todas las apariciones de un patrón en un texto: El indicador “/g” (reemplazo global) especifica el comando sed para reemplazar todas las apariciones de la cadena en la línea.
	sed 's/Microsoft Windows/GNU Linux/g' fichero.txt
Quedando como sigue:
	“La empresa tiene todos sus sistemas operativos con GNU Linux, 
	en sus últimas versiones. Con GNU Linux conseguimos alcanzar nuestros objetivos.  
	GNU Linux es una empresa que genera confianza.”

Reemplazo de la ocurrencia nth a todas las apariciones en una línea.
--------------------------------------------------------------------------------
Usamos la combinación de “/1“,” /2“, etc. y “/g” para reemplazar todos los patrones en el texto. El siguiente comando sed reemplaza la tercera, cuarta, quinta … palabra “Microsoft Windows” por “GNU Linux” en una línea.
Como sigue:
	sed 's/Microsoft Windows/GNU Linux/3g' fichero.txt

Poner entre paréntesis el primer carácter de cada palabra
----------------------------------------------------------------
Este ejemplo muestra el primer carácter de cada palabra en paréntesis:
	echo "Bienvenidos al Mundo de los Bits y los Bytes" | sed 's/\(\b[A-Z]\)/\(\1\)/g'
Con el resultado:
	(B)ienvenidos (A)l (M)undo de los (B)its y los (B)ytes

Reemplazar un string en un número de línea específico.
------------------------------------------------------------------
Podemos restringir el comando sed para reemplazar la cadena en un número de línea específico.
Por ejemplo:
	sed '3 s/Microsoft Windows/GNU Linux/' fichero.txt
	
Duplicar la línea reemplazada con el indicador “/p”
-----------------------------------------------------------
El indicador de impresión “/p” imprime la línea reemplazada dos veces en el terminal. Si una línea no tiene el patrón de búsqueda y no se reemplaza, entonces “/p” imprime esa línea solo una vez.
	$sed 's/Microsoft Windows/GNU Linux/p' fichero.txt

Reemplazar un valor entre un rango de líneas.
----------------------------------------------------
Podemos realizar un reemplazo de valores entre un rango de líneas de un fichero dado.
Por ejemplo:
	$sed '1,3 s/Microsoft Windows/GNU Linux/' fichero.txt

Borrar líneas de un fichero
------------------------------------
El comando SED se pueda utilizar para eliminar una o varias líneas de un fichero. De esta manera SED nos permite eliminar líneas de un fichero sin tener que acceder a este.
Por ejemplo, para borrar una línea en cuestión:
	Uso:
	$ sed 'nd' fichero.txt
	Ejemplo:
	$ sed '5d' fichero.txt
O bien borrar la última línea:
	$ sed '$d' fichero.txt
Entre un rango de líneas:
	sed '3,6d' fichero.txt



Conclusión
Hemos visto con unos cuantos ejemplos las ventajas de trabajar con SED para modificar ficheros en nuestro sistema operativo. Una muestra más de la increíble navaja suiza que es la terminal de comandos en GNU/Linux

Fuentes consultadas:
Geeksforgeeks.org – Sed Command in Linux (https://www.geeksforgeeks.org/sed-command-in-linux-unix-with-examples/)
