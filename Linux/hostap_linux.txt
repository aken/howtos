PROCEDIMIENTO PARA CREAR UN HOST AP CON LINUX Y PASAR LA WIFI A LA ETHERNET

sudo apt-get install hostapd

sudo nano /etc/hostapd/hostapd.conf
	interface=wlan0
	driver=nl80211
	ssid=my_hotspot
	channel=1
	hw_mode=g
	auth_algs=1
	wpa=3
	wpa_passphrase=1234567890
	wpa_key_mgmt=WPA-PSK
	wpa_pairwise=TKIP CCMP
	rsn_pairwise=CCMP
sudo apt-get install dhcp3-server

sudo nano /etc/default/isc-dhcp-server 
	INTERFACES=”wlan0″

sudo nano /etc/dhcp/dhcpd.conf 
	#option definitions common to all supported networks…
	#option domain-name “example.org”;
	#option domain-name-servers ns1.example.org, ns2.example.org;
	#default-lease-time 600;
	#max-lease-time 7200;
	subnet 10.10.0.0 netmask 255.255.255.0 {
	range 10.10.0.2 10.10.0.16;
	option domain-name-servers 8.8.4.4, 208.67.222.222;
	option routers 10.10.0.1;
	}

sudo nano /etc/network/interfaces
	auto lo
	iface lo inet loopback

	auto wlan0
	iface wlan0 inet static
	address 10.10.0.1
	netmask 255.255.255.0

sudo nano /etc/sysctl.conf 
	net.ipv4.ip_forward=1

sudo nano /etc/rc.local 
	sudo iptables -t nat -A POSTROUTING -s 10.10.0.0/16 -o eth0 -j MASQUERADE

reboot



SI NO FUNCIONA INTRODUCIR LAS SIGUIENTES LINEAS EN CONSOLA:

sudo nmcli nm wifi off
sudo rfkill unblock wlan
sudo ifconfig wlan0 10.10.0.1/24 up
sleep 1
sudo service isc-dhcp-server restart
sudo service hostapd restart

