
Permisos en Linux: chmod 777, chmod 644, chmod 755, chmod 700, chmod 666

Los tipos de permisos más comunes, o su combinación, son los siguiente:

666 ( RW / RW / RW)
Esta opción permite que todos los usuarios puedan leer y escribir en un archivo.

777 ( RWX / RWX /RWX)
Esta opción permite que todos los usuarios puedan leer, escribir y ejecutar en el archivo o carpeta

755 (RWX / RW / RW)
Con este permiso el propietario del archivo puede leer, escribir y ejecutar en el archivo mientras que los demás leer y escribir en el archivo mas no ejecutar.

644 (RW / R / R)
Con este permiso el propietario puede leer y escribir en el archivo mientras los demás solo pueden leer.

700 (RWX /---)
Con este permiso el propietario tiene el control total del archivo mientras que los demás usuarios no tendrán acceso de ningún tipo al archivo.


CALCULADORA CHMOD:
https://chmod-calculator.com/
