unset HISTFILE

Para desactivar el historial durante la sesión actual, sólo es necesario borrando la variable de entorno HISTFILE:
unset HISTFILE

De esta forma, mientras la sesión está activa es posible acceder al historial normalmente para ver el registro de los últimos comandos ejecutados, incluyendo aquellos ejecutados en la sesión actual, pero el resultado es que éstos no serán guardados en el archivo ~/.bash_history al finalizar la sesión. Sin embargo, para otras sesiones que puedan estar conviviendo simultáneamente, el comportamiento del historial será de manera tradicional. Es decir, esta configuración sólo afecta a la sesión actual.

Los comandos que no serán guardados en disco serán todos los que se ejecuten en la sesión actual, no sólo aquellos que se ejecuten luego del comando unset HISTFILE.

Existe otra técnica que consiste en agregar la opción ignorespace o ignoreboth en la variable de entorno HISTCONTROL. Esta variable controla cómo los comandos son guardados en disco (man bash). Utilizar ignorespace o ignoreboth causa que los comandos que comienzan con uno o más espacios en blanco no sean guardados en el historial. Por defecto, en la mayoría de las distribuciones Bash utiliza la variable HISTCONTROL configurada con la opción ignoredups, la cual previene que se guarden entradas duplicadas en el historial. Si se agrega la opción ignorespace, o se cambia por ignoreboth, los comandos que comienzan con espacio no se guardan en el historial.

Veamos un ejemplo:

[emi@hal9000 ~]$ echo $HISTCONTROL
ignoredups
Actualmente la variable HISTCONTROL está configurada con la opción ignoredups. Cambio ignoredups por ignoreboth:

[emi@hal9000 ~]$ HISTCONTROL="ignoreboth"
[emi@hal9000 ~]$ echo $HISTCONTROL
ignoreboth
Ejecuto algunos comandos para que se guarden en el historial:

[emi@hal9000 ~]$ ping www.google.com > /dev/null 2>&1
^C
[emi@hal9000 ~]$ pwd
/home/emi
Tanto ping como pwd se guardan en el historial:

[emi@hal9000 ~]$ history | tail -n 4
 1024  export $HISTCONTROL
 1025  ping www.google.com > /dev/null 2>&1
 1026  pwd
 1027  history | tail -n 4
Ahora ejecuto un comando que comienza con espacio en blanco y otro que no:

[emi@hal9000 ~]$  sleep 2
[emi@hal9000 ~]$ echo hola
hola
Se observa que el comando que comienza con espacio en blanco ( sleep 2) no fue guardado en el historial:

[emi@hal9000 ~]$ history | tail -n 7
 1023  echo $HISTCONTROL
 1024  export $HISTCONTROL
 1025  ping www.google.com > /dev/null 2>&1
 1026  pwd
 1027  history | tail -n 4
 1028  echo hola
 1029  history | tail -n 7


Cada técnica tiene sus ventajas y desventajas. La ventaja de borrar la variable HISTFILE (ejecutando unset HISTFILE), es que los comandos ejecutados permanecen en el historial durante la sesión actual, por lo cual es posible acceder a ellos a lo largo de toda la sesión para repetirlos o modificarlos. La desventaja es que al cerrar la sesión se pierden todos los comandos ejecutados durante la misma (ninguno se guarda en disco).

En cambio, la ventaja de utilizar la opción ignorespace o ignoreboth en la variable de entorno HISTCONTROL, es que sólo no se agregan al historial aquellos comandos específicos comienzan con espacio. Esta técnica permite controlar de manera más granular cuales comandos queremos guardar, y cuales no. La desventaja, es que estos comandos no se agregan al historial de ninguna forma, no se guardan en disco ni permanecen en el historial temporalmente durante la sesión actual. Por lo tanto, si el comando que no se desea guardar en el historial es un comando complejo construido con pipes, luego de ejecutarlo se perderá para siempre. Ni siquiera será posible acceder al mismo utilizando la flecha hacia arriba del teclado.

Es cuestión de utilizar la técnica que mejor se adapte a nuestras necesidades.
