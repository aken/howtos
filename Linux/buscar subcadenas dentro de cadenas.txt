Usar caracteres comodín
El enfoque más simple es rodear la subcadena con el asterisco (asterisco) * símbolos comodín y compararlo con la cadena. El comodín es un símbolo que se utiliza para representar cero, uno o más caracteres.
Si la prueba devuelve verdadero, la subcadena está contenida en la cadena.

En el siguiente ejemplo, estamos usando la instrucción if y el operador de igualdad ( == ) para verificar si la subcadena SUB está dentro de la cadena STR:
#!/bin/bash
	STR='GNU/Linux is an operating system'
	SUB='Linux'
	if [[ "$STR" == *"$SUB"* ]]; then
		echo "Exists!"
	fi
Una vez ejecutado, el script generará:
Exists!

Usando el asistente social
En lugar de usar la instrucción if, también puede usar la instrucción case para verificar si una cadena incluye otra cadena o no.
#!/bin/bash
	STR='GNU/Linux is an operating system'
	SUB='Linux'
	case $STR in *"$SUB"*)
		echo -n "Exists!"
	;;
	esac

Usando el operador Regex
Otra opción para determinar si una subcadena especificada ocurre dentro de una cadena es usar el operador regex = ~. Cuando se utiliza este operador, la cadena correcta se trata como una expresión regular.
El punto seguido de un asterisco . * Coincide con cero o más ocurrencias de cualquier carácter excepto un carácter de nueva línea.
#!/bin/bash
	STR='GNU/Linux is an operating system'
	SUB='Linux'
	if [[ "$STR" =~.*"$SUB".* ]]; then
		echo "Exists!"
	fi
El script devolverá el siguiente mensaje:
Exists!

Usando Grep
El comando grep también se puede utilizar para buscar cadenas en otra cadena.
En el siguiente ejemplo, estamos pasando la cadena $ STR como entrada a grep y comprobando si la cadena $ SUB está dentro de la cadena de entrada. El comando devolverá verdadero o falso.
#!/bin/bash
	STR='GNU/Linux is an operating system'
	SUB='Linux'
	if grep -q "$SUB" <<< "$STR"; then
		echo "Exists!"
	fi
La opción -q le dice a grep que omita la salida.

