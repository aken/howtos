Para guardar las sesiones ssh que se realizan en los servidores, hay que realizar los siguientes cambios:

Según la máquina es un archivo u otro
	vim /etc/bash.bashrc
	vim /etc/bashrc

al final del fichero añadimos:
	export PROMPT_COMMAND='RETRN_VAL=$?;logger -p local6.debug "$(whoami) ${HOSTNAME}:${PWD} [$$]: $(history 1 | sed "s/^[ ]*[0-9]\+[ ]*//" ) [$RETRN_VAL]"'

creamos fichero log y cambiamos propietario y permisos:
	touch /var/log/cmdhistory.log
	chown syslog:adm /var/log/cmdhistory.log
	chmod 640 /var/log/cmdhistory.log

Logging de sesiones SFTP

creamos fichero vim /usr/local/bin sftp-wrapper con este contenido:
	#!/bin/bash
	# sftpd wrapper script pre/post sesiones
	#
	# acciones pre sesion y logging
	SOURCE_IP=${SSH_CLIENT%% *}
	MSG_SESSION_START="user $LOGNAME session start from $SOURCE_IP"
	logger -p local5.notice -t sftpd-wrapper -i "$MSG_SESSION_START"
	# comienzo actual sesion SFTP
	/usr/lib/openssh/sftp-server -f LOCAL5 -l VERBOSE
	# añadimos acciones post sesion aqui
	MSG_SESSION_STOP="user $LOGNAME session ended from $SOURCE_IP"
	logger -p local5.notice -t sftpd-wrapper -i "$MSG_SESSION_STOP"

Cambiamos los permisos al fichero:
	chmod +x /usr/local/bin/sftp-wrapper

Modificamos el fichero /etc/ssh/sshd_config:
	# override default of no subsystems
	#Subsystem sftp /usr/lib/openssh/sftp-server
	Subsystem sftp /usr/local/bin/sftp-wrapper

Creamos el fichero /var/log/sftp.log y modificamos permisos y propietario:
	touch /var/log/sftp.log
	chmod 640 /var/log/sftp.log
	chown syslog:adm /var/log/sftp.log

Editamos el fichero
	vim /etc/rsyslog.conf
y añadimos al final:
	##MONITOR CMD y SFTP
	local6.* /var/log/cmdhistory.log
	local5.* /var/log/sftp.log

Se incluyen ambos ficheros en el logrotate de la máquina y para ello, creamos fichero /etc/logrotate.d/sftp_cmd
	vim /etc/logrotate.d/sftp_cmd
y añadimos el contenido:
	/var/log/sftp.log
	/var/log/cmdhistory.log {
		 rotate 52
		 weekly
		 missingok
		 notifempty
		 compress
		 delaycompress
		 create 640 syslog adm
		 sharedscripts
		 postrotate
			   invoke-rc.d rsyslog rotate >/dev/null 2>&1 || true
		 endscript }

Reiniciamos servicios:
	systemctl restart sshd
	systemctl restart rsyslog
