
Hay veces que por los motivos que sean un usuario tiene que tener acceso a un sistema y en él sólo debería de poder permanecer en su ~home y también debería de sólo poder ejecutar los comandos que se le indiquen. Esto funciona muy bien excepto por la curiosidad. Si un usuario tiene acceso a algo, intentará tarde o temprano verlo. Entonces... ¿cómo podemos evitar al usuario cotilla? La respuesta es sencilla, realizando una jaula chroot para su sesión y que no pueda salir de ella ni ejecutar comandos fuera de los que le estén permitidos. A continuación os voy a enseñar cómo montar un sistema 'chroot' y lo pongo entre comillas, por que realmente no es en sí una jaula propiamente dicha.

Partimos de que necesitamos crear un usuario test que sólo tenga acceso al sistema para realizar un telnet a otra máquina de la red. Puesto que únicamente necesita ejecutar un telnet necesitamos restringir el resto de comandos disponibles en una sesión bash normal. Vamos a ir paso a paso configurando todo.

Creamos el usuario test
Este será un usuario normal del sistema, así que lo creamos como un usuario normal. La única peculiaridad es que cambiamos la shell de dicho usuario. Por defecto suele ser /bin/bash y vamos a establecer /bin/rbash. rbash es realmente una copia de bash, pero realmente es un "restricted bash".
	shell> adduser --shell /bin/rbash test

Creamos el fichero .bash_profile
Hay que crear este fichero en la home del usuario que se ha creado y para el que queremos aplicar los permisos. El contenido del fichero será el que sigue,
	if [ -f ~/.bashrc ]; then
	   . ~/.bashrc
	fi

	PATH=$HOME/apps
	export PATH

Evitamos las modificaciones
Cuando ya tengamos el fichero creado, impedimos que nadie pueda realizar modificaciones en el fichero.
	shell> chattr +i /home/test/.bash_profile

Creamos el directorio de apps e instalamos los programas con 'acceso'
Ahora una vez que tenemos ya todo configurado sólo queda crear el directorio apps y dentro de él, crear un link a los programas que deseamos que el usuario tenga permisos. Todos los programas que estén dentro de apps, el usuarios los podrá ejecutar, sino, no.
	shell> mkdir apps
	shell> ln -s /usr/bin/telnet /home/test/apps/

Comprobamos que funciona
Accedemos al sistema y comprobamos que funciona correctamente.
	shell> ssh test@remote
	test@remote's password:
	shell@remote> ls
	-rbash: ls: no se encontró la orden
	shell@remote> cd
	-rbash: cd: no se encontró la orden
	shell@remote> telnet
	telnet>
