
En este procedimiento se describirá los pasos a seguir para poder autenticarse en maquinas con sistema operativo Ubuntu, CentOS u Oracle con usuarios del directorio Activo.

Configuración del servidor con sistema operativo Ubuntu.

Instalación de paquetes.

UBUNTU:
	apt-get -y install libpam-radius-auth
CENTOS:
	sudo yum install epel-release
	sudo yum update
	sudo yum install pam_radius
ORACLE LINUX:
	sudo wget http://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm
	sudo rpm -ivh epel-release-6-8.noarch.rpm
	sudo yum install pam_radius
ORACLE LINUX 7:
	sudo yum install -y wget
	sudo wget https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
	sudo yum install epel-release-latest-7.noarch.rpm
	sudo yum install -y pam_radius


Creación de usuarios
Para que la autenticacion contra el directorio activo funcione se debe crear un  usuario local con el mismo nombre de inicio de sesión.
	sudo useradd usuario -c "Apellidos, Nombre" -m -s /bin/bash

Edición de Archivos.
/etc/pam_radius_auth.conf
	#añadimos la linea con la ip del servidor radius y la clave secreta (IP [clave compartida] 3)
	127.0.0.1       secret             1
	IP				[clave compartida] 3
	other-server    other-secret       3

/etc/pam.d/sshd 
	#Añadimos la siguiente linea en el grupo de lineas con auth
UBUNTU:
	auth sufficient pam_radius_auth.so
CENTOS / ORACLE:
	auth  sufficient  /usr/lib64/security/pam_radius_auth.so

/etc/pam.d/sudo
Aunque configuremos este archivo y el archivo /etc/pam.d/su para que los usuario puedan hacer sudo y su con sus cuentas del directorio activo deberemos añadir a dichos usuario o grupos locales de la maquina al archivo /etc/sudoers
	#Añadimos la siguiente linea antes de los @include common-auth y demás
UBUNTU:
	auth sufficient pam_radius_auth.so
CENTOS / ORACLE:
	auth  sufficient  /usr/lib64/security/pam_radius_auth.so

/etc/pam.d/su
	#Añadimos la siguiente linea antes de los @include common-auth y demás
UBUNTU:
	auth sufficient pam_radius_auth.so
CENTOS / ORACLE:
	auth  sufficient  /usr/lib64/security/pam_radius_auth.so
Hay casos en los que el servidor al ejecutar el comando "su" pide dos veces la autenticación, si esto pasa, para solucionarlo habrá que deshacer este paso.

/etc/ssh/sshd_config
	#En caso de que este fichero tenga descomentada la linea AllowGroups , hay que añadir el grupo de administradores
	...
	AllowGroups ... grupoadmin
	...

Creación de clientes en el servidor RADIUS
Una vez hemos accedido al servidor radius (Windows) en Network Policy Server hacemos boton derecho sobre RADIUS Clients | New.
En la pantalla que nos aparecerá debemos rellenar los siguientes campos:
Friendly name: Nombre del cliente, en nuestro caso lo mas recomendable seria el nombre de la maquina.
Address (IP or DNS): IP o DNS de la maquina que queremos añadir.
Shared Secret: Introducir la contraseña compartida.

