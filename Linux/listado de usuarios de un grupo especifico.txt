
#VARIABLES EXTERNAS PARA EL COMANDO AWK
users=$(getent group users | cut -d: -f3)

#COMENZAMOS EL PROCESO DE LISTADO
sudo echo -e "-----------------------------------" > ./$HOSTNAME.listadoUsuarios
sudo echo -e "-----------------------------------" >> ./$HOSTNAME.listadoUsuarios
sudo echo -e $HOSTNAME >> ./$HOSTNAME.listadoUsuarios
sudo echo -e "-----------------------------------" >> ./$HOSTNAME.listadoUsuarios
sudo echo -e "-----------------------------------" >> ./$HOSTNAME.listadoUsuarios


#OBTENEMOS USUARIOS GRUPO users
sudo echo -e USUARIOS users >> ./$HOSTNAME.listadoUsuarios
sudo echo -e "-----------------------------------" >> ./$HOSTNAME.listadoUsuarios
sudo awk -v users=$users -F: '{if($4 == users) print $1"\t             "$5}' /etc/passwd >> ./$HOSTNAME.listadoUsuarios
sudo echo -e "-----------------------------------" >> ./$HOSTNAME.listadoUsuarios

#iconv ./$HOSTNAME.listadoUsuarios -f iso-8859-1 -t utf-8 -o ./$HOSTNAME.listadoUsuarios

