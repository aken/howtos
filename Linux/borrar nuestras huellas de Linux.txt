Borrar nuestras huellas

 

Este paso es importante, ya que de nada nos habr� servido habernos introducido en el sistema y haber conseguido el nivel de root si al d�a siguiente nos han cortado el acceso debido a que hemos dejado huellas por todas partes.

El sistema operativo Unix guarda varios registros (logs) de las conexiones de los usuarios al sistema. Existen varios ficheros y comandos que ayudan al administrador a conocer todos los detalles acerca de las conexiones de los usuarios. Aparte de estos ficheros y comandos, existen diversas facilidades y aplicaciones que realizan un registro continuado y exhaustivo acerca de las actividades del usuario dentro del sistema.

Ficheros:
(Cuando pongo dos directorios significa que el fichero puede estar en cualquiera de esos dos directorios).

utmp

Guarda un registro (log) de los usuarios que est�n utilizando el sistema mientras est�n conectados a �l.

Directorios:  /Var/Adm./utmp   /etc/utmp

wtmp
Guarda un log cada vez que un usuario se introduce en el sistema o sale del sistema.

Directorios:  /Var/Adm./wtmp   /etc/wtmp

lastlog
Guarda un log del momento exacto en que un usuario entr� por �ltima vez.

Directorio:  /Var/Adm./lastlog

acct
Registra todos los comandos ejecutados por cada usuario (aunque no registra los argumentos con que dichos comandos fueron ejecutados).

Directorio:  /Var/adm/acct

En algunos sistemas el fichero acct se puede llamar pacct



Comandos:

who
Permite saber qui�n est� conectado al sistema en el momento en que ejecutamos el comando.

finger
Lo mismo que el comando who, con el a�adido de que se puede aplicar a otras m�quinas. Es decir, podemos saber qu� usuarios est�n conectados a una determinada m�quina en el momento en que ejecutamos el comando.

users
Igual que el who

rusers
Igual que finger, pero la m�quina remota debe utilizar el sistema operativo Unix.

Los comandos who, finger, users y rusers toman la informaci�n que sacan en pantalla del fichero utmp.

last
Permite saber cuando fue la �ltima vez que se conect� un usuario.

El comando last toma la informaci�n que saca en pantalla del fichero wtmp.

ps
Permite saber qu� procesos est�n siendo ejecutados por el sistema y que usuarios los ejecutan.

El comando ps ofrece una informaci�n mucho m�s completa de qui�n est� utilizando el sistema puesto que un usuario que no aparezca en los ficheros utmp o wtmp puede tener procesos ejecut�ndose, por lo que el comando ps ofrecer� la informaci�n de qui�n est� ejecutando dichos procesos. En contrapartida, la informaci�n que ofrece el comando ps es m�s complicada de interpretar que la informaci�n ofrecida por el resto de comandos.

accton
Activa un proceso llamado accounting, que es el que proporciona informaci�n al fichero acct.

lastcomm
Permite saber qu� comandos han ejecutado los usuarios.

acctcom
Igual que lastcomm pero exclusivamente para Unix del tipo SYSTEM V.

Los comandos lastcomm y acctcom toman la informaci�n que sacan por pantalla del fichero acct (pacct en algunos sistemas).



Por lo tanto, si queremos borrar nuestras huellas del sistema, bastar� con borrar cualquier log relativo a nuestro usuario de los ficheros utmp, wtmp y acct. Esto se puede hacer de dos formas:

Ficheros utmp y wtmp:

1 - No borramos los ficheros pero los dejamos con cero bytes. S�lo se utiliza como �ltimo recurso por suscitar muchas sospechas por parte de los administradores. Hay hackers que opinan que esto es incluso peor que no borrar los logs.

2 - Los ficheros utmp y wtmp no son ficheros de texto, es decir, no se pueden editar con un editor de textos. Sin embargo, existen programas llamados zappers (debido a que el programa m�s famoso de este tipo se llama zap) que pueden borrar los datos relativos a un usuario en particular de estos ficheros dejando el resto de los datos relativo a los dem�s usuarios intacto.

Fichero acct:

Cuando el accounting est� activado (es decir, cuando el sistema recoge informaci�n acerca de los comandos ejecutados en el fichero acct) es bastante complicado borrar nuestras huellas, de hecho no se pueden borrar del todo, aunque s� se pueden reducir a una m�nima informaci�n de nuestra presencia en el sistema.

1 - Lo primero que hacemos nada m�s entrar en el sistema es copiar el fichero acct a otro fichero y lo ultimo que hacemos antes de abandonar el sistema es copiar dicho fichero de nuevo al acct, de modo que los comandos que hemos ejecutado durante la sesi�n no aparecen en el fichero acct.

Problema: Nuestra entrada en el sistema queda registrada, as� como las dos copias.

2 - Dejamos el fichero acct a cero bytes. Como antes, esto es bastante sospechoso para un administrador, adem�s, algunos sistemas reaccionan mal y paran el proceso de accounting, para no levantar sospechas habr�a que reactivarlo con el comando accton.

Problema: Bastante sospechoso. El propio comando accton quedar�a registrado como ejecutado por nuestro usuario.

3 - Hacerse un editor para el fichero acct que borrara los datos correspondientes a nuestro usuario y dejara intactos los datos relativos al resto de los usuarios. Existen unos pocos programas que hacen esto.

Problema: La ejecuci�n del programa editor que borra nuestras huellas quedar�a registrado como ejecutado por nuestro usuario.

Afortunadamente, no hay muchos sistemas que tengan activado el accounting debido a la cantidad de capacidad que es necesaria para guardar los comandos ejecutados por cada usuario.

Aparte de los ficheros utmp, wtmp, acct y lastlog, hay que tener en cuenta otras facilidades y aplicaciones que posee el sistema operativo Unix que permiten al administrador vigilar ciertos aspectos cr�ticos relativos a la seguridad y al mantenimiento del sistema.

1 - Syslog
Syslog es una aplicaci�n que viene con el sistema operativo Unix. El sistema operativo Unix se puede configurar de tal forma que determinados programas, procesos o aplicaciones generen mensajes que son enviados a determinados ficheros donde quedan registrados dichos mensajes. Estos mensajes son generados cuando se dan unas determinadas condiciones, ya sean condiciones relativas a seguridad, mantenimiento o simplemente de tipo puramente informativo.

Para conseguir esto hay que configurar varias cosas:

A - Decidir qu� programas, procesos y aplicaciones pueden generar mensajes. (Pongo los principales)

kern: mensajes relativos al kernel.
user:  mensajes relativos a procesos ejecutados por usuarios normales.
mail:  mensajes relativos al sistema de correo.
lpr:    mensajes relativos a impresoras.
auth:  mensajes relativos a programas y procesos de autentificaci�n (aquellos en los que est�n involucrados nombres de usuarios y passwords, por ejemplo login, su, getty, etc).
daemon:  mensajes relativos a otros demonios del sistema.

B - Decidir qu� tipos de mensajes pueden generar cada uno de esos programas, procesos o aplicaciones.

emerg:   emergencias graves.
alert:   problemas que deben ser solucionados con urgencia.
crit:   errores cr�ticos.
err:   errores ordinarios.
warning:   avisos.
notice:   cuando se da una condici�n que no constituye un error pero a la que se le debe dar una cierta atenci�n.
info:   mensajes informativos.

C - Decidir a qu� ficheros van a parar dichos mensajes dependiendo del tipo al que pertenezca el mensaje correspondiente.

Syslog cumple su funci�n mediante el syslogd (syslog daemon o en castellano el demonio syslog).

Nota: un demonio (o daemon) es un proceso que no tiene propietario (es decir, no es ejecutado por ning�n usuario en particular) y que se est� ejecutando permanentemente.

El syslogd lee su configuraci�n del fichero /etc/syslog.conf  Dicho fichero contiene la configuraci�n relativa a qu� eventos del sistema son registrados y en qu� ficheros son registrados. Los ficheros a los cuales se mandan los registros (logs) pueden estar situados en la misma m�quina en la que estamos trabajando o en otra m�quina de la red.

C�mo borrar las huellas relativas al syslog:

Bien, nuestras andanzas por el sistema cuando hemos accedido a �l y cuando nos hemos convertido en root, pueden generar diversos mensajes registrados por el syslogd y guardados en los ficheros indicados en el /etc/syslog.conf

A diferencia de los ficheros utmp, wtmp, acct y lastlog, los ficheros en los que se guardan los registros del syslog s� se pueden editar con un editor de textos.

Para poder borrar estas huellas necesitamos tener privilegios de root, naturalmente. Bastar� con examinar el fichero /etc/syslog.conf para saber los ficheros que guardan los registros del syslog. Despu�s miraremos cada uno de esos ficheros comprobando que no hay ning�n mensaje relativo a nuestra intrusi�n en el sistema (los mensajes del estilo "login: Root LOGIN REFUSED on ttya" a ciertas horas de la noche son bastante sospechosos :-) ). En caso de que lo haya, lo borramos y cambiamos la fecha del fichero con el comando touch de forma que coincida la fecha del �ltimo mensaje (despu�s de haber borrado nuestras huellas) con la fecha del fichero. Si no lo hacemos as�, alg�n administrador demasiado suspicaz puede comprobar que las fechas no coinciden y deducir que alguien ha modificado el fichero (esta es una precauci�n extrema pero la recomiendo por experiencia). Si es necesario, y s�lo si es necesario, habr�a que cambiar la fecha de los directorios en los que est�n incluidos los ficheros que guardan los logs.

Si en el fichero /etc/syslog.conf hay mensajes que se destinan a /dev/console eso significa que los mensajes (ya sean de error, alerta o emergencia) salen directamente en la pantalla del root (o sea, en la consola). En este caso no se puede hacer nada (que yo sepa), pero mensajes de este tipo suelen estar generados por alertas bastante graves como por ejemplo intentar acceder con la cuenta de root directamente o utilizar el comando su para intentar convertirse en root, etc. Es decir, cuanto m�s sigilosos seamos a la hora de hacernos root y menos ruido armemos m�s posibilidades tendremos de no aparecer en este tipo de logs.

2 - TCP-Wrapper
Se trata de una aplicaci�n que proporciona una serie de mecanismos para el registro (logging) y filtro (filtering) de aquellos servicios invocados o llamados a trav�s del inetd (internet daemon). Con esta herramienta el administrador posee un control absoluto de las conexiones hacia y desde su m�quina.

Puede, entre otras muchas cosas, filtrar un servicio de internet como por ejemplo el telnet, ftp, etc de forma que nadie pueda conectarse al sistema desde otra m�quina o puede especificar una lista de m�quinas que s� pueden conectarse (y las dem�s no podr�n). Adem�s, el administrador es informado en todo momento y con todo lujo de detalles de las conexiones que se han hecho desde su m�quina y hacia su m�quina con cualquiera de los diferentes servicios de internet (telnet, ftp, finger, etc...)

Como en el syslog, para borrar nuestras huellas del tcp-wrapper, tendremos que buscar posibles huellas mirando el archivo de configuraci�n (alojado normalmente en el directorio /etc), borrar dichas huellas y cambiar las fechas de los ficheros correspondientes.

Bien, hasta aqu� un resumen sobre c�mo borrar las huellas. Como ver�is me he extendido un poco m�s porque me parece importante que la gente adquiera conciencia de que tan importante o m�s que controlar el sistema (convertirse en root) es saber ocultarse en �l (aunque es una opini�n personal).

Puede parecer bastante pesado el borrar todas las posibles huellas que hayamos dejado, pero en algunas ocasiones, una vez que hayamos visto los ficheros de configuraci�n es posible preparar un shell script (el equivalente a los ficheros batch en MS-DOS, aunque la programaci�n en shell es infinitamente m�s potente :-) ) que haga todo el trabajo por nosotros en cuesti�n de borrar las huellas. Dicho script lo podemos dejar bien camuflado en el sistema para que la pr�xima vez que entremos lo podamos ejecutar (utilizando como par�metros el usuario con el que hayamos entrado, el terminal por el que hayamos entrado, la hora a la que hayamos entrado, etc..) ahorr�ndonos todo el trabajo pesado.

Para terminar con lo de borrar las huellas, s�lo advertir que aunque seamos perfectamente invisibles en el sistema, cualquier usuario que est� conectado al mismo tiempo que nosotros podr�a detectarnos viendo el terminal por el que hemos entrado (el fichero /dev/ correspondiente a nuestro terminal tendr�a como propietario (owner) al usuario con el que hemos entrado en el sistema, y la fecha del fichero /dev/ correspondiente al terminal tambi�n nos delatar�a). Para evitar esto tendr�amos que cambiar de owner el fichero correspondiente al terminal (teniendo privilegios de root naturalmente) al owner que tengan los otros terminales a los cuales no hay nadie conectado (es decir, al owner de los terminales por defecto que normalmente es el root).

De todas formas, esto �ltimo, junto con lo de cambiar de fecha ciertos ficheros de logs, son medidas quiz� extremas, pero vuelvo a insistir que son muy recomendables.

Por �ltimo, la cuesti�n de ocultar o camuflar procesos mientras los estamos ejecutando es otra cuesti�n que se tratar� en otro mensaje si ten�is la paciencia de seguir. :-)

Ya hemos visto de forma resumida y sin detallar algunas t�cnicas sobre c�mo conseguir acceso, conseguir privilegios y borrar nuestras huellas. Vamos a ver el �ltimo paso, c�mo conseguir acceso a otros ordenadores una vez controlado el host que hayamos hackeado (es decir, despu�s de asegurarnos que hemos borrado absolutamente todas nuestras huellas y de implantar alg�n sushi u otro m�todo an�logo para conseguir privilegios de root).

Una vez controlado el host que ten�amos como objetivo, podemos hacer todo lo que queramos en el sistema, aunque hay que tener en cuenta que nuestras acciones pueden ser registradas por el syslog, tcp-wrapper u otra utilidad que genere logs, por lo que cuando vayamos a irnos del sistema siempre tendremos que comprobar antes que no hemos dejado registros (logs).

Es en este punto donde adquiere importancia la "filosof�a" del hacker. La diferencia entre un hacker y un cracker (no me estoy refiriendo a alguien que rompe las protecciones de software), consiste en que un cracker accede al sistema para da�arlo o corromperlo y un hacker accede al sistema simplemente para conseguir informaci�n o por pura curiosidad, pero nunca corromper� ni borrar� ning�n fichero del sistema, sigue el lema (aunque tampoco de forma radical, es decir, sin tom�rselo al pie de la letra) de "se ve pero no se toca". A esto �ltimo hay que hacer una excepci�n , naturalmente. Los �nicos ficheros que el hacker modificar� o borrar� ser�n los ficheros relativos a los logs que haya podido dejar en el sistema. Por supuesto que esto es una situaci�n ideal y no realista, en la pr�ctica un hacker puede que realice otras acciones en el sistema que puedan modificar ficheros ya existentes, pero siempre procurar� que los cambios sean m�nimos.