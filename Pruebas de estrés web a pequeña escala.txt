************
************ HTTPERF   (http://www.hpl.hp.com/research/linux/httperf/)
************

Para instalarlo
# yum install httperf
# apt-get install httperf

Un ejemplo de como realizar un test de carga:
$ httperf --server 192.168.1.10 --port 80 --uri /index.html --rate 300 --num-conn 30000 --num-call 1 --timeout 5
(httperf descargara http://192.168.1.10/index.html repetidamente 300 veces por segundo con un total de 30000 peticiones)

Las opciones que permite httperf son:
–server: El hostname de la web ha realizar el test.
–uri: La url de la pagina que se abrirá.
–rate: Cuantas peticiones se quieren enviar por segundo.
–num-conn: El total de conexiones que se abrirán.
–num-call: Cuantas peticiones se enviaran por conexión.
–timeout: Cuantos segundos ha de esperar para que se considere que la petición se ha perdido.


************
************ ApacheBench
************

Esta herramienta que forma parte de apache (no es necesario instalar apache, instalando el apache-utils tirando de apt-get nos valdrá) nos permite crear una cantidad concreta de peticiones al servidor web para ver como este responde y finalmente nos devolverá una estadística de las peticiones hechas y el tiempo que ha tardado el servidor en responderlas.

Funciona asi:

ab -n 150 -c 30 http(s)://www.midominio.com/

Donde la n es el número de peticiones que se van a realizar y la c las peticione recurrentes simultáneas. Empezará a testear un buen rato, durante el cual os recomiendo que probeis a acceder al servidor, a ver que responde, controlad los recursos, etc, para haceros una idea. Y finalmente nos mostrará las estadísticas del testeo. Y poco mas.
