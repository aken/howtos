
Primero nos conectamos al servidor SMTP remoto por telnet en el puerto 25:
        $telnet servidor_remoto 25

Recibiremos un mensaje similar al que sigue:
        Trying 200.40.31.8…
        Connected to servidor_remoto.
        Escape character is ‘^]’.
        220 servidor_remoto ESMTP

Luego debemos saludar al servidor con HELO o EHLO:
        helo servidor_remoto
        250 mta01.servidor_remoto

con el comando MAIL FROM indicamos la dirección de correo de quien envía:
        MAIL FROM: enrique@servidor_remoto
        250 2.1.0 Ok

Luego indicaremos el destinatario del email con RCPT TO (si hay varios destinatarios se pondrán varios RCPT TO):
        RCPT TO:usuario@dominio.dom
        250 Accepted

Con el comando DATA comenzamos a armar nuestro correo:
        DATA
        354 Enter message, ending with «.» on a line by itself

Aquí debemos tomar en cuenta que las primeras líneas que debemos mandar son los encabezados que serán visibles para el usuario, como From>

        From:'Juan Perez'<juan_perez@dominio.com>
        To:'Usuario1'<usuario1@dominio.dom>
        Cc:'Usuario2'<usuario2@dominio.dom>
        Cc:'Usuario3'<usuario3@dominio.dom>
        Subject: Hola este es un correo de prueba
        Aqui escribo el cuerpo del email y cuando termino – en una linea en blando – digito un punto «.»
        .
        250 OK id=1bhdeY-0000Vb-FO

Listo! El correo ha sido enviado
