# Primero creamos un disco de 250gb para usar con la TimeMachine
hdiutil create -size 250g -type SPARSEBUNDLE -fs "HFS+J" TimeMachine.sparsebundle
# Una vez creado lo movemos a la unidad de red que deseemos y tras montarlo lo activamos
sudo tmutil setdestination /Volumes/TimeMachine
