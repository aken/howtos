Comprobar SSL/TLS en servidores de correo (SMTP IMAP POP)

Ahora con la importancia de tener conexiones seguras, muchos han decidido cambiar y usar protocolos SSL o TLS en su servidor de correo y otros se han visto obligados ya que hay cliente de correo que sólo soportan ese tipo de conexiones.
Para conectar con un servicio que no tiene SSL/TLS podemos usar telnet:
    IMAP: telnet mail.dominios.es 143
    POP3:  telnet mail.dominio.es 110SMTP: telnet mail.dominio.es 25

Con eso podremos comprobar que la conexión al servidor se realiza sin problemas, e incluso podemos llegar a mandar correos y consultarlos.
Cuando tenemos un servicio bajo SSL/TLS, el telnet no será capaz de conectar por lo que tendremos que usar el cliente de openssl. En la mayor parde de distros de linux debería estar instalado, si no, con una búsqueda rápida en los repositorios lo encontrarás.

Para comprobar que funciona, y además mostrar el certificado (para comprobar si es válido o autofirmado):
    SSL
		IMAP: openssl s_client -showcerts -connect mail.dominios.es:995
		POP3: openssl s_client -showcerts -connect mail.dominios.es:993
		SMTP: openssl s_client -showcerts -connect mail.dominios.es:465
    TLS
		IMAP: openssl s_client -showcerts -connect mail.dominios.es:143 -starttls imap
		POP: openssl s_client -showcerts -connect mail.dominios.es:110 -starttls pop3
		SMTP: openssl s_client -showcerts -connect mail.dominios.es:25 -starttls smtp

Con eso podremos conectar y revisar que todo vaya bien.

Comprobación del registro SPF:
spfquery -sender=<mail del dominio> -ip=<ip servidor correo> -helo=<nombre host>
