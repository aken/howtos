Detener trabajo SQL

En SQL Server Management Studio ejecutar el comando:
EXEC msdb.dbo.sp_stop_job @job_name = 'nombre de proceso';

Por ejemplo, para detener el proceso de carga de mercancias:
EXEC msdb.dbo.sp_stop_job @job_name = 'ProcesoBDCargaMercancias';
