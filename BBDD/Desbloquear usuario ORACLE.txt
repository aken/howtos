Por defecto, en ORACLE 11g, si los usuarios se crean bajo el ROLE Default, estarán sujetos a una política en la cual la contraseña caducará a los 180 días.
Esto es bueno puesto que te obliga a cambiar la contraseña. El problema es cuando uno desconoce esta política o se le olvida cambiar la contraseña en ese tiempo y el usuario, pasado ese periodo, se bloquea con sus correspondientes consecuencias para la aplicación que lo usa.
Para poder ver si en nuestra base de datos tenemos activa esta política ejecutamos la siguiente consulta:

SQL> select * from dba_profiles;

El resultado que obtenemos en este caso el siguiente:
DEFAULT;COMPOSITE_LIMIT;KERNEL;UNLIMITED
DEFAULT;SESSIONS_PER_USER;KERNEL;UNLIMITED
DEFAULT;CPU_PER_SESSION;KERNEL;UNLIMITED
DEFAULT;CPU_PER_CALL;KERNEL;UNLIMITED
DEFAULT;LOGICAL_READS_PER_SESSION;KERNEL;UNLIMITED
DEFAULT;LOGICAL_READS_PER_CALL;KERNEL;UNLIMITED
DEFAULT;IDLE_TIME;KERNEL;UNLIMITED
DEFAULT;CONNECT_TIME;KERNEL;UNLIMITED
DEFAULT;PRIVATE_SGA;KERNEL;UNLIMITED
DEFAULT;FAILED_LOGIN_ATTEMPTS;PASSWORD;10
DEFAULT;PASSWORD_LIFE_TIME;PASSWORD;180
DEFAULT;PASSWORD_REUSE_TIME;PASSWORD;UNLIMITED
DEFAULT;PASSWORD_REUSE_MAX;PASSWORD;UNLIMITED
DEFAULT;PASSWORD_VERIFY_FUNCTION;PASSWORD;NULL
DEFAULT;PASSWORD_LOCK_TIME;PASSWORD;1
DEFAULT;PASSWORD_GRACE_TIME;PASSWORD;7

Como podemos observa vemos que para el ROLE Defaut la política de expiración de password está puesta a 180 días. Si nuestros usuarios tienen este ROLE asociado, están sujetos a esta política.
Si queremos verificar a qué profile está asociado nuestro usuario, lo podemos hacer con la siguiente consulta:
	select username ,profile from dba_users;

Para cambiar esta política en el ROLE DEFAULT y establecerla a indefinida permitiéndonos así a nosotros cambiar la contraseña del usuario cuando creamos conveniente ejecutaremos la siguiente consulta:
	ALTER PROFILE DEFAULT LIMIT PASSWORD_LIFE_TIME UNLIMITED;

En caso de que nuestro usuario haya sido bloqueado debido a que ya pasaron los 180 días desde su creación o cambio de contraseña, podremos desbloquearlo y cambiar la contraseña de la siguiente forma
	SQL> alter user usuario identified by "password";
	SQL> alter user usuario account unlock;
